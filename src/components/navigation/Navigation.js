import React from "react";
import logo from "./logo.png";

const Navigation = () => {
  return (
    <div className="navigation">
      <div className="navigation__button navigation__button--left">
        <i className="fas fa-bars" />
      </div>

      <div className="navigation__logo">
        <img src={logo} alt="Timber Logo" />
      </div>

      <div className="navigation__button navigation__button--right">
        <i className="fa fa-comments" />
      </div>
    </div>
  );
};

export default Navigation;
