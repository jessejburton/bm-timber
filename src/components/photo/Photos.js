import React from "react";
import Photo from "./Photo";

const Photos = () => {
  return (
    <div className="photos">
      <Photo classes="tilt-3" />
      <Photo classes="tilt-2" />
      <Photo classes="tilt-1" />
      <Photo classes="tilt-0" />
    </div>
  );
};

export default Photos;
