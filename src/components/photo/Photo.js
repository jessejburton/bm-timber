import React from "react";

const Photo = props => {
  const { classes } = props;

  const photoClass = `photo ${classes}`;

  return (
    <div className={photoClass}>
      <div className="photo__image">
        <div className="photo__image-container">
          <img src="images/profile.jpg" />
          <div className="photo__overlay row">
            <div className="photo__name col-1-of-2">
              <strong>Brittany</strong>, 25
            </div>
            <div className="photo__pictures col-1-of-2">
              3 <i className="fa fa-camera" />
            </div>
          </div>
        </div>
        <div className="photo__details row">
          <div className="col-1-of-2">
            <div className="photo__button">
              <i className="fa fa-user-friends" /> <span>24</span>
            </div>
          </div>
          <div className="col-1-of-2">
            <div className="photo__button no-left-margin">
              <i className="fa fa-book-open" /> <span>6</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Photo;
