import React from "react";

const LikeButtons = () => {
  return (
    <div className="like">
      <div className="like__button like__button--info">
        <button className="like__button--inside">
          <i className="fa fa-info" />
        </button>
      </div>
      <div className="like__button like__button--dislike">
        <button className="like__button--inside">
          <i className="fa fa-times" />
        </button>
      </div>
      <div className="like__button like__button--like">
        <button className="like__button--inside">
          <i className="fa fa-check" />
        </button>
      </div>
    </div>
  );
};

export default LikeButtons;
