import React, { Component } from "react";
import "./App.css";
import Navigation from "./components/navigation/Navigation";
import Photos from "./components/photo/Photos";
import LikeButtons from "./components/buttons/LikeButtons";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navigation />
        <Photos />
        <LikeButtons />
      </div>
    );
  }
}

export default App;
